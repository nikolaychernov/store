package com.nikolaychernov.store.ui.products.details

import com.nikolaychernov.store.RxImmediateSchedulerRule
import com.nikolaychernov.store.data.ProductRepository
import com.nikolaychernov.store.model.Product
import com.nikolaychernov.store.util.LocalizationUtils
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ProductDetailsPresenterTest {

    @Rule
    @JvmField
    val mOverrideSchedulersRule = RxImmediateSchedulerRule()

    private val productRepository = mockk<ProductRepository>()
    private val localizationUtils = mockk<LocalizationUtils>(relaxed = true)
    private val product = Product("Shirt", 20.0, null)

    private val viewState = mockk<`ProductDetailsView$$State`>(relaxed = true)

    private lateinit var presenter: ProductDetailsPresenter

    @Before
    fun setup() {
        presenter = ProductDetailsPresenter(productRepository, localizationUtils).apply {
            setViewState(this@ProductDetailsPresenterTest.viewState)
        }
    }

    @Test
    fun loadingError_ShouldShowErrorAndClose() {
        every { productRepository.getProduct(any()) } throws Exception("Error")

        presenter.loadProduct(0)

        verifyLoadingError()
    }

    @Test
    fun loadingEmpty_ShouldShowErrorAndClose() {
        every { productRepository.getProduct(any()) } returns null

        presenter.loadProduct(0)

        verifyLoadingError()
    }

    private fun verifyLoadingError() {
        verify {
            viewState.showError(any())
            viewState.closeScreen()
        }
    }

    @Test
    fun savingEmptyDescription_ShouldShowError() {
        enterProperties("", "")
        presenter.saveClicked()

        verify(exactly = 0) {
            viewState.showDescription(any())
            viewState.showPrice(any())
        }
        verifySavingError()
    }

    private fun enterProperties(description: String, price: String) {
        presenter.descriptionChanged(description)
        presenter.priceChanged(price)
    }

    @Test
    fun savingEmptyPrice_ShouldShowError() {
        enterProperties("Shirt", "")
        presenter.saveClicked()

        verify { viewState.showDescription("Shirt") }
        verify(exactly = 0) {
            viewState.showPrice(any())
        }
        verifySavingError()
    }

    @Test
    fun savingInvalidPrice_ShouldShowError() {
        enterProperties("Shirt", "Price")
        presenter.saveClicked()

        verify {
            viewState.showDescription("Shirt")
            viewState.showPrice("Price")
        }
        verifySavingError()
    }

    private fun verifySavingError() {
        verify {
            viewState.showError(any())
        }
        verify(exactly = 0) {
            productRepository.create(any())
            viewState.closeScreen()
        }
    }

    @Test
    fun savingValidProduct() {
        setupRepositorySuccess()

        enterProperties("Shirt", "2.0")
        presenter.saveClicked()

        verify {
            viewState.showDescription("Shirt")
            viewState.showPrice("2.0")
        }
        verifySaving(null)
    }

    @Test
    fun savingValidProductWithImage() {
        setupRepositorySuccess()

        presenter.imagePicked("Uri")
        enterProperties("Shirt", "2.0")
        presenter.saveClicked()

        verify { viewState.showImage("Uri") }
        verifySaving(image = "Uri")
    }

    private fun setupRepositorySuccess() {
        every { productRepository.getProduct(any()) } returns product
        every { productRepository.update(any()) } returns Unit
        presenter.loadProduct(1)
    }

    private fun verifySaving(image: String?) {
        verify {
            productRepository.update(match { it.image == image })
            viewState.closeScreen()
        }
        verify(exactly = 0) {
            viewState.showError(any())
        }
    }

    @Test
    fun savingValidProductWithInvalidImagePicked() {
        setupRepositorySuccess()

        presenter.imagePicked("")
        enterProperties("Shirt", "2.0")
        presenter.saveClicked()

        verify {
            viewState.showError(any())
            productRepository.update(match { it.image == null })
            viewState.closeScreen()
        }
        verify(exactly = 0) {
            viewState.showImage(any())
        }
    }

    @Test
    fun repositoryException_ShouldShowError() {
        every { productRepository.getProduct(any()) } returns product
        every { productRepository.update(any()) } throws Exception("Error")

        presenter.loadProduct(1)
        presenter.imagePicked("")
        enterProperties("Shirt", "2.0")
        presenter.saveClicked()

        verify {
            viewState.showError(any())
            productRepository.update(match { it.image == null })
        }
        verify(exactly = 0) {
            viewState.closeScreen()
            viewState.showImage(any())
        }
    }
}