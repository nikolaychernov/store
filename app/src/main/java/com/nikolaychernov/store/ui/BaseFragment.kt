package com.nikolaychernov.store.ui

import com.nikolaychernov.store.dagger.Injectable
import moxy.MvpAppCompatFragment

open class BaseFragment : MvpAppCompatFragment(), Injectable