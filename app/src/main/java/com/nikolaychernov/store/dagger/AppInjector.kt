package com.nikolaychernov.store.dagger

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.nikolaychernov.store.StoreApplication
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection

object AppInjector {

    @JvmStatic
    fun init(application: StoreApplication): AppComponent {
        val appComponent = DaggerAppComponent.builder()
            .application(application)
            .build()
        appComponent.inject(application)
        application
            .registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
                override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                    handleActivity(activity)
                }

                override fun onActivityStarted(activity: Activity) {
                    //do nothing
                }

                override fun onActivityResumed(activity: Activity) {
                    //do nothing
                }

                override fun onActivityPaused(activity: Activity) {
                    //do nothing
                }

                override fun onActivityStopped(activity: Activity) {
                    //do nothing
                }

                override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {
                    //do nothing
                }

                override fun onActivityDestroyed(activity: Activity) {
                    //do nothing
                }
            })
        return appComponent
    }

    private fun handleActivity(activity: Activity) {
        if (activity is Injectable) {
            AndroidInjection.inject(activity)
        }
        if (activity is FragmentActivity) {
            activity.supportFragmentManager
                .registerFragmentLifecycleCallbacks(
                    object : FragmentManager.FragmentLifecycleCallbacks() {

                        override fun onFragmentPreCreated(
                            fm: FragmentManager,
                            f: Fragment,
                            savedInstanceState: Bundle?
                        ) {
                            super.onFragmentPreCreated(fm, f, savedInstanceState)
                            if (f is Injectable) {
                                AndroidSupportInjection.inject(f)
                            }
                        }
                    }, true
                )
        }
    }
}