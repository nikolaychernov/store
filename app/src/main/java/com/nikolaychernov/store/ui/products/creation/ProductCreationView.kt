package com.nikolaychernov.store.ui.products.creation

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

interface ProductCreationView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showImage(imageUriString: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showError(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun closeScreen()
}
