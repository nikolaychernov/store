package com.nikolaychernov.store.dagger

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.nikolaychernov.store.data.ProductRepository
import com.nikolaychernov.store.db.StoreDb
import com.nikolaychernov.store.ui.products.creation.ProductCreationPresenter
import com.nikolaychernov.store.ui.products.details.ProductDetailsPresenter
import com.nikolaychernov.store.ui.products.list.ProductListPresenter
import com.nikolaychernov.store.util.LocalizationUtils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class AppModule {

    @Provides
    @Singleton
    open fun provideApplicationContext(application: Application): Context =
        application.applicationContext

    @Provides
    @Singleton
    open fun provideResources(application: Application): Resources = application.resources

    @Provides
    @Singleton
    open fun provideDb(application: Application): StoreDb =
        StoreDb.getInstance(application)

    @Provides
    @Singleton
    open fun provideProductRepository(storeDb: StoreDb): ProductRepository =
        ProductRepository(storeDb.productDao())

    @Provides
    @Singleton
    open fun provideProductListPresenter(
        productRepository: ProductRepository,
        localizationUtils: LocalizationUtils
    ): ProductListPresenter =
        ProductListPresenter(productRepository, localizationUtils)

    @Provides
    open fun provideProductDetailsPresenter(
        productRepository: ProductRepository,
        localizationUtils: LocalizationUtils
    ): ProductDetailsPresenter =
        ProductDetailsPresenter(
            productRepository,
            localizationUtils
        )

    @Provides
    open fun provideProductCreationPresenter(
        productRepository: ProductRepository,
        localizationUtils: LocalizationUtils
    ): ProductCreationPresenter =
        ProductCreationPresenter(
            productRepository,
            localizationUtils
        )
}