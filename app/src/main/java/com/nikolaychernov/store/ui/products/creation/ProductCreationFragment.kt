package com.nikolaychernov.store.ui.products.creation

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.nikolaychernov.store.ui.BaseFragment
import com.nikolaychernov.store.util.hideKeyboard
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.product_details_fragment.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject


class ProductCreationFragment : BaseFragment(), ProductCreationView {

    override fun showImage(imageUriString: String) {
        image.setImageURI(Uri.parse(imageUriString))
    }

    override fun closeScreen() {
        findNavController().navigateUp()
    }

    override fun showError(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    @Inject
    lateinit var daggerPresenter: ProductCreationPresenter

    @InjectPresenter
    lateinit var presenter: ProductCreationPresenter

    @ProvidePresenter
    fun providePresenter(): ProductCreationPresenter {
        return daggerPresenter
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            com.nikolaychernov.store.R.layout.product_details_fragment,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.setOnClickListener {
            presenter.saveClicked(edit_description.text.toString(), edit_price.text.toString())
        }
        image.setOnClickListener {
            openImagePicker(it.context)
        }
    }

    private fun openImagePicker(context: Context) {
        CropImage.activity()
            .setFixAspectRatio(true)
            .start(context, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                val resultUri = result.uri
                presenter.imagePicked(resultUri.toString())
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        activity.hideKeyboard()
    }
}