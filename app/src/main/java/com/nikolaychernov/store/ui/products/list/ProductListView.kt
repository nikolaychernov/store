package com.nikolaychernov.store.ui.products.list

import com.nikolaychernov.store.model.Product
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

interface ProductListView : MvpView {

    @StateStrategyType(AddToEndStrategy::class)
    fun showProducts(products: List<Product>)

    @StateStrategyType(AddToEndStrategy::class)
    fun setEmptyViewVisible(visible: Boolean)

    @StateStrategyType(AddToEndStrategy::class)
    fun setProductsVisible(visible: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError(message: String)

}
