package com.nikolaychernov.store.dagger

import com.nikolaychernov.store.ui.MainActivity
import com.nikolaychernov.store.ui.products.creation.ProductCreationFragment
import com.nikolaychernov.store.ui.products.details.ProductDetailsFragment
import com.nikolaychernov.store.ui.products.list.ProductListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ViewModule {

    @ContributesAndroidInjector
    internal abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun contributeProductListFragment(): ProductListFragment

    @ContributesAndroidInjector
    internal abstract fun contributeProductDetailsFragment(): ProductDetailsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeProductCreationFragment(): ProductCreationFragment

}
