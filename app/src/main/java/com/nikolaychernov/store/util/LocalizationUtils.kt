package com.nikolaychernov.store.util

import android.content.res.Resources
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalizationUtils @Inject constructor(private val resources: Resources) {

    fun getString(resource: Int): String = resources.getString(resource)

}
