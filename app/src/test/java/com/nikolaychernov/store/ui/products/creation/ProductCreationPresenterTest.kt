package com.nikolaychernov.store.ui.products.creation

import com.nikolaychernov.store.RxImmediateSchedulerRule
import com.nikolaychernov.store.data.ProductRepository
import com.nikolaychernov.store.util.LocalizationUtils
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ProductCreationPresenterTest {

    @Rule
    @JvmField
    val mOverrideSchedulersRule = RxImmediateSchedulerRule()

    private val productRepository = mockk<ProductRepository>()
    private val localizationUtils = mockk<LocalizationUtils>(relaxed = true)

    private val viewState = mockk<`ProductCreationView$$State`>(relaxed = true)

    private lateinit var presenter: ProductCreationPresenter

    @Before
    fun setup() {
        presenter = ProductCreationPresenter(productRepository, localizationUtils).apply {
            setViewState(this@ProductCreationPresenterTest.viewState)
        }
    }

    @Test
    fun savingEmptyDescription_ShouldShowError() {
        presenter.saveClicked("", null)

        verifyError()
    }

    @Test
    fun savingEmptyPrice_ShouldShowError() {
        presenter.saveClicked("Shirt", null)

        verifyError()
    }

    @Test
    fun savingInvalidPrice_ShouldShowError() {
        presenter.saveClicked("Shirt", "Price")

        verifyError()
    }

    private fun verifyError() {
        verify {
            viewState.showError(any())
        }
        verify(exactly = 0) {
            productRepository.create(any())
            viewState.closeScreen()
        }
    }

    @Test
    fun savingValidProduct() {
        every { productRepository.create(any()) } returns 0

        presenter.saveClicked("Shirt", "2.0")

        verifySaving(null)
    }

    @Test
    fun savingValidProductWithImage() {
        every { productRepository.create(any()) } returns 0

        presenter.imagePicked("Uri")
        presenter.saveClicked("Shirt", "2.0")

        verify { viewState.showImage("Uri") }
        verifySaving(image = "Uri")
    }

    private fun verifySaving(image: String?) {
        verify {
            productRepository.create(match { it.image == image })
            viewState.closeScreen()
        }
        verify(exactly = 0) {
            viewState.showError(any())
        }
    }

    @Test
    fun savingValidProductWithInvalidImagePicked() {
        every { productRepository.create(any()) } returns 0

        presenter.imagePicked("")
        presenter.saveClicked("Shirt", "2.0")

        verify {
            viewState.showError(any())
            productRepository.create(match { it.image == null })
            viewState.closeScreen()
        }
        verify(exactly = 0) {
            viewState.showImage(any())
        }
    }

    @Test
    fun repositoryException_ShouldShowError() {
        every { productRepository.create(any()) } throws Exception("Error")

        presenter.imagePicked("")
        presenter.saveClicked("Shirt", "2.0")

        verify {
            viewState.showError(any())
            productRepository.create(match { it.image == null })
        }
        verify(exactly = 0) {
            viewState.closeScreen()
            viewState.showImage(any())
        }
    }
}