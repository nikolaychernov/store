package com.nikolaychernov.store.ui.products.list

import com.nikolaychernov.store.R
import com.nikolaychernov.store.data.ProductRepository
import com.nikolaychernov.store.util.LocalizationUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class ProductListPresenter(
    private val productRepository: ProductRepository,
    private val localizationUtils: LocalizationUtils
) :
    MvpPresenter<ProductListView>() {

    private var disposable: Disposable? = null

    fun loadProducts() {
        disposable = productRepository.getProducts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.isEmpty()) {
                    viewState.setEmptyViewVisible(true)
                    viewState.setProductsVisible(false)
                } else {
                    viewState.setEmptyViewVisible(false)
                    viewState.setProductsVisible(true)
                    viewState.showProducts(it)
                }
            }, {
                viewState.showError(
                    it.localizedMessage ?: localizationUtils.getString(R.string.error_loading)
                )
            })
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }
}