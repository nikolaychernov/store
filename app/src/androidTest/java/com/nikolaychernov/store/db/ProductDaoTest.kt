package com.nikolaychernov.store.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.nikolaychernov.store.model.Product
import io.reactivex.subscribers.TestSubscriber
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ProductDaoTest {

    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var storeDb: StoreDb
    private lateinit var productDao: ProductDao

    private val product1 = Product("Shirt", 20.0, null)
    private val product2 = Product("Shirt", 20.0, null)
    private val product3 = Product("Shirt", 20.0, null)
    private val products: List<Product> = listOf(
        product1,
        product2,
        product3
    )

    @Before
    @Throws(Exception::class)
    fun initDb() {
        storeDb = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context,
            StoreDb::class.java
        )
            .allowMainThreadQueries()
            .build()

        productDao = storeDb.productDao()
    }

    @After
    @Throws(Exception::class)
    fun closeDb() {
        storeDb.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun onInit_productsShouldBeEmpty() {
        val testSubscriber = TestSubscriber<List<Product>>()
        productDao.getProducts()
            .subscribe(testSubscriber)

        testSubscriber.assertValuesOnly(emptyList())
    }

    @Test
    @Throws(InterruptedException::class)
    fun productsLoadingAfterInsert() {
        val testSubscriber = TestSubscriber<List<Product>>()
        productDao.getProducts()
            .subscribe(testSubscriber)
        productDao.insert(products)

        testSubscriber.assertValuesOnly(emptyList(), products)
    }

    @Test
    @Throws(InterruptedException::class)
    fun productInsertAndDelete() {
        val testSubscriber = TestSubscriber<List<Product>>()
        productDao.getProducts()
            .subscribe(testSubscriber)
        productDao.insert(product1)
        val product = productDao.getProduct(1)
        productDao.delete(product!!)

        testSubscriber.assertValuesOnly(emptyList(), listOf(product1), emptyList())
    }

    @Test
    @Throws(InterruptedException::class)
    fun deletingNotExistingProduct() {
        val testSubscriber = TestSubscriber<List<Product>>()
        productDao.getProducts()
            .subscribe(testSubscriber)
        productDao.delete(product1)

        testSubscriber.assertValuesOnly(emptyList())
    }

    @Test
    @Throws(InterruptedException::class)
    fun loadingNotExistingProduct() {
        productDao.insert(product2)
        val product = productDao.getProduct(product1.id)

        assertEquals(null, product)
    }

    @Test
    @Throws(InterruptedException::class)
    fun updatingProduct() {
        productDao.insert(product1)
        val product = productDao.getProduct(1)

        assertNotNull(product)

        product?.apply {
            description = "new"
            productDao.update(this)
        }

        assertEquals("new", productDao.getProduct(product!!.id)?.description)
    }
}