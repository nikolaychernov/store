package com.nikolaychernov.store.ui.products.details

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.nikolaychernov.store.R
import com.nikolaychernov.store.ui.BaseFragment
import com.nikolaychernov.store.util.hideKeyboard
import com.nikolaychernov.store.util.onChange
import com.nikolaychernov.store.util.placeCursorAtEnd
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.product_details_fragment.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject

class ProductDetailsFragment : BaseFragment(),
    ProductDetailsView {

    override fun showImage(imageUriString: String) {
        image.setImageURI(Uri.parse(imageUriString))
    }

    override fun closeScreen() {
        findNavController().navigateUp()
    }

    override fun showError(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    override fun showPrice(price: String) {
        edit_price.setText(price)
        edit_price.placeCursorAtEnd()
    }

    override fun showDescription(description: String) {
        edit_description.setText(description)
        edit_description.placeCursorAtEnd()
    }

    @Inject
    lateinit var daggerPresenter: ProductDetailsPresenter

    @InjectPresenter
    lateinit var presenter: ProductDetailsPresenter

    @ProvidePresenter
    fun providePresenter(): ProductDetailsPresenter {
        return daggerPresenter
    }

    private val args: ProductDetailsFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.loadProduct(args.productId)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(
            R.layout.product_details_fragment, container, false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.setOnClickListener {
            presenter.saveClicked()
        }
        image.setOnClickListener {
            openImagePicker(it.context)
        }
        edit_description.onChange{
            presenter.descriptionChanged(it)
        }
        edit_price.onChange {
            presenter.priceChanged(it)
        }
    }

    private fun openImagePicker(context: Context) {
        CropImage.activity()
            .setFixAspectRatio(true)
            .start(context, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                presenter.imagePicked(result.uri.toString())
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.product_details, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.delete -> {
                presenter.deleteClicked()
                true
            }
            else -> false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        activity.hideKeyboard()
    }
}