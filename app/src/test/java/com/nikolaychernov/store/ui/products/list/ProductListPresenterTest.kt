package com.nikolaychernov.store.ui.products.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nikolaychernov.store.RxImmediateSchedulerRule
import com.nikolaychernov.store.data.ProductRepository
import com.nikolaychernov.store.model.Product
import com.nikolaychernov.store.util.LocalizationUtils
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ProductListPresenterTest {

    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val mOverrideSchedulersRule = RxImmediateSchedulerRule()

    private val productRepository = mockk<ProductRepository>()
    private val localizationUtils = mockk<LocalizationUtils>()

    private val productListViewState = mockk<`ProductListView$$State`>(relaxed = true)

    private lateinit var presenter: ProductListPresenter

    private val product1 = Product("Shirt", 20.0, null)
    private val products: List<Product> = listOf(
        product1
    )

    @Before
    fun setup() {
        presenter = ProductListPresenter(productRepository, localizationUtils).apply {
            setViewState(productListViewState)
        }
    }

    @Test
    fun loadEmptyList_ShouldShowEmptyView() {
        every { productRepository.getProducts() } returns Flowable.just(emptyList())

        presenter.loadProducts()

        verify {
            productListViewState.setEmptyViewVisible(true)
            productListViewState.setProductsVisible(false)
        }
        verify(exactly = 0) {
            productListViewState.showError(any())
            productListViewState.showProducts(any())
        }
    }

    @Test
    fun loadProducts_ShouldShowProducts() {
        every { productRepository.getProducts() } returns Flowable.just(products)

        presenter.loadProducts()

        verify {
            productListViewState.setEmptyViewVisible(false)
            productListViewState.setProductsVisible(true)
            productListViewState.showProducts(products)
        }
        verify(exactly = 0) {
            productListViewState.showError(any())
        }
    }

    @Test
    fun exceptionWhenLoading_ShouldShowError() {
        every { productRepository.getProducts() } returns Flowable.error(Exception("error"))

        presenter.loadProducts()

        verify {
            productListViewState.showError("error")
        }
        verify(exactly = 0) {
            productListViewState.setEmptyViewVisible(any())
            productListViewState.setProductsVisible(any())
            productListViewState.showProducts(any())
        }
    }
}