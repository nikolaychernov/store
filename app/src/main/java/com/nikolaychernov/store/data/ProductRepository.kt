package com.nikolaychernov.store.data

import com.nikolaychernov.store.db.ProductDao
import com.nikolaychernov.store.model.Product
import io.reactivex.Flowable

class ProductRepository(private val productDao: ProductDao) {
    //тут предполагается что прячет от верхних слоев, из сети или из локальной базы тянуть

    fun getProducts(): Flowable<List<Product>> = productDao.getProducts()

    fun getProduct(id: Long) = productDao.getProduct(id)

    fun create(product: Product) = productDao.insert(product)

    fun update(product: Product) = productDao.update(product)

    fun delete(product: Product) = productDao.delete(product)

}