package com.nikolaychernov.store.ui.products.creation

import com.nikolaychernov.store.R
import com.nikolaychernov.store.data.ProductRepository
import com.nikolaychernov.store.model.Product
import com.nikolaychernov.store.util.LocalizationUtils
import com.nikolaychernov.store.util.ValidationException
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpPresenter


@InjectViewState
class ProductCreationPresenter(
    private val productRepository: ProductRepository,
    private val localizationUtils: LocalizationUtils
) :
    MvpPresenter<ProductCreationView>() {

    private var disposable: CompositeDisposable = CompositeDisposable()
    private var uri: String? = null

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }

    fun saveClicked(description: String?, price: String?) {
        try {
            if (description.isNullOrEmpty())
                throw ValidationException(localizationUtils.getString(R.string.empty_description_error))
            if (price.isNullOrEmpty())
                throw ValidationException(localizationUtils.getString(R.string.empty_price_error))
            val priceDouble = price.toDouble()
            val productToInsert =
                Product(
                    description = description,
                    price = priceDouble,
                    image = uri
                )
            disposable.add(
                Completable.fromAction { productRepository.create(productToInsert) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { viewState.closeScreen() },
                        { throwable: Throwable ->
                            viewState.showError(
                                throwable.localizedMessage
                                    ?: localizationUtils.getString(R.string.cant_save)
                            )
                        })
            )
        } catch (e: ValidationException) {
            viewState.showError(
                e.localizedMessage
                    ?: localizationUtils.getString(R.string.cant_save)
            )
        } catch (e: NumberFormatException) {
            viewState.showError(localizationUtils.getString(R.string.incorrect_price_format))
        }
    }

    fun imagePicked(selectedImage: String?) {
        if (selectedImage.isNullOrBlank()) {
            viewState.showError("No image was selected")
        } else {
            uri = selectedImage
            viewState.showImage(selectedImage)
        }
    }
}