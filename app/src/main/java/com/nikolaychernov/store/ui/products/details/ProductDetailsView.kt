package com.nikolaychernov.store.ui.products.details

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

interface ProductDetailsView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showPrice(price: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showDescription(description: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showError(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun closeScreen()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showImage(imageUriString: String)
}
