package com.nikolaychernov.store.ui.products.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.nikolaychernov.store.model.Product
import com.nikolaychernov.store.ui.BaseFragment
import kotlinx.android.synthetic.main.product_list_fragment.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject
import androidx.recyclerview.widget.DividerItemDecoration


class ProductListFragment : BaseFragment(),
    ProductListView {
    override fun showError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun showProducts(products: List<Product>) {
        productsAdapter.submitList(products)
    }

    override fun setEmptyViewVisible(visible: Boolean) {
        nothing_found.isVisible = visible
    }

    override fun setProductsVisible(visible: Boolean) {
        list_products.isVisible = visible
    }

    @Inject
    lateinit var daggerPresenter: ProductListPresenter

    @InjectPresenter
    lateinit var presenter: ProductListPresenter

    @ProvidePresenter
    fun providePresenter(): ProductListPresenter {
        return daggerPresenter
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(
            com.nikolaychernov.store.R.layout.product_list_fragment,
            container,
            false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.loadProducts()
    }

    private val productsAdapter = ProductsAdapter {
        findNavController().navigate(
            ProductListFragmentDirections.actionProductListFragmentToProductDetailsFragment(
                it.id
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val linearLayoutManager = LinearLayoutManager(activity)
        list_products.layoutManager = linearLayoutManager
        list_products.adapter = productsAdapter
        fab_add_product.setOnClickListener {
            findNavController().navigate(com.nikolaychernov.store.R.id.action_productListFragment_to_productCreationFragment)
        }
    }
}