package com.nikolaychernov.store.ui.products.details

import com.nikolaychernov.store.R
import com.nikolaychernov.store.data.ProductRepository
import com.nikolaychernov.store.model.Product
import com.nikolaychernov.store.util.LocalizationUtils
import com.nikolaychernov.store.util.ValidationException
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpPresenter


@InjectViewState
class ProductDetailsPresenter(
    private val productRepository: ProductRepository,
    private val localizationUtils: LocalizationUtils
) :
    MvpPresenter<ProductDetailsView>() {

    private var disposable: CompositeDisposable = CompositeDisposable()
    private lateinit var product: Product
    private var description: String = ""
    private var price: String = ""

    fun loadProduct(productId: Long) {
        if (!::product.isInitialized) {
            disposable.add(
                Single.fromCallable { productRepository.getProduct(productId) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it != null) {
                            product = it
                            this.description = it.description
                            this.price = it.price.toString()
                            viewState.showDescription(description)
                            viewState.showPrice(price)
                            it.image?.apply {
                                viewState.showImage(this)
                            }
                        } else {
                            viewState.showError(localizationUtils.getString(R.string.no_such_product))
                            viewState.closeScreen()
                        }
                    }, { throwable: Throwable ->
                        run {
                            viewState.showError(
                                throwable.localizedMessage
                                    ?: localizationUtils.getString(R.string.no_such_product)
                            )
                            viewState.closeScreen()
                        }
                    })
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }

    fun deleteClicked() {
        disposable.add(
            Completable.fromAction { productRepository.delete(product) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { viewState.closeScreen() },
                    { throwable: Throwable ->
                        viewState.showError(
                            throwable.localizedMessage
                                ?: localizationUtils.getString(R.string.cant_delete)
                        )
                    })
        )
    }

    fun saveClicked() {
        try {
            if (description.isEmpty())
                throw ValidationException(localizationUtils.getString(R.string.empty_description_error))
            if (price.isEmpty())
                throw ValidationException(localizationUtils.getString(R.string.empty_price_error))
            val priceDouble = price.toDouble()
            val productToInsert: Product = product.also {
                it.description = description
                it.price = priceDouble
            }
            disposable.add(
                Completable.fromAction { productRepository.update(productToInsert) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { viewState.closeScreen() },
                        { throwable: Throwable ->
                            viewState.showError(
                                throwable.localizedMessage
                                    ?: localizationUtils.getString(R.string.cant_save)
                            )
                        })
            )
        } catch (e: ValidationException) {
            viewState.showError(
                e.localizedMessage ?: localizationUtils.getString(R.string.cant_delete)
            )
        } catch (e: NumberFormatException) {
            viewState.showError(localizationUtils.getString(R.string.incorrect_price_format))
        }
    }

    fun imagePicked(selectedImage: String?) {
        if (selectedImage.isNullOrBlank()) {
            viewState.showError("No image was selected")
        } else {
            product.image = selectedImage
            viewState.showImage(selectedImage)
        }
    }

    fun descriptionChanged(description: String) {
        if (description != this.description) {
            this.description = description
            viewState.showDescription(description)
        }
    }

    fun priceChanged(price: String) {
        if (price != this.price) {
            this.price = price
            viewState.showPrice(price)
        }
    }
}