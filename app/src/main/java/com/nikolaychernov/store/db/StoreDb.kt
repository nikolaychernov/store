package com.nikolaychernov.store.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.nikolaychernov.store.model.Product


@Database(
    entities = [Product::class],
    version = 1,
    exportSchema = false
)
abstract class StoreDb : RoomDatabase() {

    abstract fun productDao(): ProductDao

    companion object {

        @Volatile
        private var INSTANCE: StoreDb? = null

        fun getInstance(context: Context): StoreDb =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context): StoreDb {
            return Room.databaseBuilder(
                context.applicationContext,
                StoreDb::class.java, "controller.db"
            )
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}