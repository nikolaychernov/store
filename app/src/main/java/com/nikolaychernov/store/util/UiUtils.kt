package com.nikolaychernov.store.util

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

fun Activity?.hideKeyboard() {
    if (this != null) {
        val view = currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}

fun EditText.onChange(cb: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            cb(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            //No need implement here
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            //No need implement here
        }
    })
}

fun EditText.placeCursorAtEnd() {
    setSelection(text.length)
}
