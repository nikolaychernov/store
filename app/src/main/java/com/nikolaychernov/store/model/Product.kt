package com.nikolaychernov.store.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Product(
    var description: String,
    var price: Double,//предположим что одна валюта
    var image: String? = null
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}