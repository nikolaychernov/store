package com.nikolaychernov.store.db

import androidx.room.*

@Dao
abstract class BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(obj: T): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(objects: List<T>): List<Long>

    @Update
    abstract fun update(obj: T)

    @Update
    abstract fun update(objects: List<T>)

    @Transaction
    open fun upsert(obj: T) {
        val id = insert(obj)
        if (id == -1L) {
            update(obj)
        }
    }

    @Transaction
    open fun upsert(objects: List<T>) {
        val insertResult = insert(objects)
        val updateList = mutableListOf<T>()
        insertResult.forEachIndexed { index, value ->
            if (value == -1L) {
                updateList += objects[index]
            }
        }
        if (updateList.isNotEmpty()) {
            update(updateList)
        }
    }

    @Delete
    abstract fun delete(obj: T)

    @Delete
    abstract fun delete(objects: List<T>)
}