package com.nikolaychernov.store.db

import androidx.room.Dao
import androidx.room.Query
import com.nikolaychernov.store.model.Product
import io.reactivex.Flowable

@Dao
abstract class ProductDao : BaseDao<Product>() {

    @Query("SELECT * FROM Product")
    abstract fun getProducts(): Flowable<List<Product>>

    @Query("SELECT * FROM Product WHERE id = :id")
    abstract fun getProduct(id: Long): Product?

}