package com.nikolaychernov.store.ui.products.list

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nikolaychernov.store.R
import com.nikolaychernov.store.model.Product
import kotlinx.android.synthetic.main.product_list_item.view.*


class ProductsAdapter(private val clickListener: (Product) -> Unit) :
    ListAdapter<Product, ProductsAdapter.ViewHolder>(object : DiffUtil.ItemCallback<Product>() {
        override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem == newItem
        }

        override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem.id == newItem.id
        }
    }) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflater.inflate(
                R.layout.product_list_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(product: Product, clickListener: (Product) -> Unit) {
            itemView.text_description.text = product.description
            itemView.text_price.text = product.price.toString()
            product.image?.apply {
                itemView.imageView.setImageURI(Uri.parse(this))
            }
            itemView.setOnClickListener { clickListener(product) }
        }
    }
}