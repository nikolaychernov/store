package com.nikolaychernov.store.dagger

import android.app.Application
import com.nikolaychernov.store.StoreApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        AndroidInjectionModule::class,
        ViewModule::class]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun appModule(module: AppModule): Builder

        fun build(): AppComponent
    }

    fun inject(application: StoreApplication)
}