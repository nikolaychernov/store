package com.nikolaychernov.store

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.facebook.stetho.Stetho
import com.nikolaychernov.store.dagger.AppInjector
import com.nikolaychernov.store.log.CrashlyticsTree
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.fabric.sdk.android.Fabric
import timber.log.Timber
import javax.inject.Inject

open class StoreApplication : Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        Fabric.with(this, Crashlytics())
        Timber.plant(Timber.DebugTree(), CrashlyticsTree())

        AppInjector.init(this)
    }
}