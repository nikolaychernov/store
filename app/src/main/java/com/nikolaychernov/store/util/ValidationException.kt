package com.nikolaychernov.store.util

class ValidationException(override val message: String?) : Exception()